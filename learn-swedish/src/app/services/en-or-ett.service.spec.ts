import { TestBed } from '@angular/core/testing';

import { EnOrEttService } from './en-or-ett.service';

describe('EnOrEttService', () => {
  let service: EnOrEttService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EnOrEttService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
