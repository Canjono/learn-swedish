import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Word, wordGenus } from '../models/words';

@Injectable({
  providedIn: 'root'
})
export class EnOrEttService {

  constructor() { }

  getWords(): Observable<Word[]> {
    const words = [];

    words.push(new Word('gaffel', wordGenus.en));
    words.push(new Word('hus', wordGenus.ett));
    words.push(new Word('man', wordGenus.en));
    words.push(new Word('kvinna', wordGenus.en));
    words.push(new Word('stol', wordGenus.en));
    words.push(new Word('tåg', wordGenus.ett));

    return of(words);
  }
}
