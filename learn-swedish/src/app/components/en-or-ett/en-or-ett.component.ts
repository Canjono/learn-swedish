import { Component, OnInit } from '@angular/core';
import { Word } from 'src/app/models/words';
import { EnOrEttService } from 'src/app/services/en-or-ett.service';

@Component({
  selector: 'app-en-or-ett',
  templateUrl: './en-or-ett.component.html',
  styleUrls: ['./en-or-ett.component.scss']
})
export class EnOrEttComponent implements OnInit {
  words: Word[];
  currentWord: Word;
  currentWordIndex: number;
  correctAnswers = 0;
  wrongAnswers = 0;
  totalAnswers = 0;

  constructor(
    private enOrEttService: EnOrEttService
  ) { }

  ngOnInit(): void {
    this.getWords();
  }

  private getWords(): void {
    this.enOrEttService.getWords()
      .subscribe(
        (words: Word[]) => {
          this.words = words;
          this.nextWord();
        },
        error => {
          console.log('error:', error);
        }
      );
  }

  onAnswer(genus: string): void {
    if (genus === this.currentWord.genus) {
      console.log('correct!');
      this.correctAnswers++;
    } else {
      console.log('wrong!');
      this.wrongAnswers++;
    }

    this.totalAnswers++;
    this.nextWord();
  }

  private nextWord(): void {
    this.currentWordIndex = !this.currentWord || this.currentWordIndex === this.words.length - 1
      ? 0
      : this.currentWordIndex + 1;

    this.currentWord = this.words[this.currentWordIndex];
  }

}
