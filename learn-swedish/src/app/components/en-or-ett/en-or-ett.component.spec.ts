import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnOrEttComponent } from './en-or-ett.component';

describe('EnOrEttComponent', () => {
  let component: EnOrEttComponent;
  let fixture: ComponentFixture<EnOrEttComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnOrEttComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnOrEttComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
