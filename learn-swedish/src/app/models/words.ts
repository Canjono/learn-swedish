export class Word {
  name: string;
  genus: wordGenus;

  constructor(name: string, genus: wordGenus) {
    this.name = name;
    this.genus = genus;
  }
}

export enum wordGenus {
  en = 'en',
  ett = 'ett'
}
